import React from 'react';
import { connect } from 'react-redux';

import { createSelector } from 'reselect';

class App extends React.Component {
	
	constructor(props) {
		super(props);
  }

	render() {
    console.log(this.props.item._id);
		return (
      <li>
        <span>{this.props.item._id}</span> / <span>{this.props.item.name}</span>
      </li>
		);
	}
}

const getItem = (state, props) => {
  return state[props.id];
};

const makeGetItemState = () => {
  return createSelector(
    [ getItem ],
    (item) => item
  );
}

// const getItemState = createSelector(
//   [ getItem ],
//   (item) => item
// );



const makeMapStateToProps = () => {
  const getItemState = makeGetItemState();
  const mapStateToProps = (state, props) => {
    return {
      item: getItemState(state, props)
    }
  }
  return mapStateToProps;
}


export default connect(makeMapStateToProps)(App);
// export default connect(mapStateToProps)(App);



