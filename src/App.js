import React from 'react';
import Store from './reducers';
import { Provider } from 'react-redux';

import List from './List';

class App extends React.Component {
	
	constructor(props) {
		super(props);
  }

	render() {
		return (
      <Provider store={Store}>
        <List />
      </Provider>
		);
  }

}

export default App;
