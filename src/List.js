import React from 'react';
import { connect } from 'react-redux';  

import Item from './Item';

class App extends React.Component {
	
	constructor(props) {
		super(props);
    }

    loadData = () => {
        fetch('./jsontest.json').then(response => {
            return response.json();
        }).then(data => {
            const pp = 5;
            const offset = Object.keys(this.props.state).length;
            const newData = data.slice(offset, offset + pp);
            this.props.dispatch({
                'payload': newData,
                'type': 'LOAD_DATA'
            });
        }).catch(err => {
            console.log("Error Reading data " + err);
        }); 
    }
  
    componentDidMount() {
        this.loadData();
    }

	render() {
        console.log(this.props.state);
        const keys = Object.keys(this.props.state);
		return (
            <div className="App">
                <button onClick={this.loadData}>load more</button>
                
                <ul>
                {keys.map((key) =>
                    <Item key={key} id={key} />
                )}
                </ul>
            </div>
		);
	}
}

const mapStateToProps = (state) => {
  return {
    state: state
  };
};

export default connect(mapStateToProps)(App);
