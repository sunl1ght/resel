import { createStore } from 'redux';
const initialState = {};

export default createStore((state = initialState, action) => {
    switch (action.type) {
        case 'LOAD_DATA':
            let newState = { ...state};
            [ ...action.payload ].map((item) => {
                newState[item._id] = { ...item };
            });

            return newState;
        default:
            return state;
    }
});
